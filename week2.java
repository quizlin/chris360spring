package Java;

import static org.junit.Assert.*;

public class assertArrayEquals {


    public void main() {

        String[] expectedArray = {"one", "two", "three"};

        String[] resultArray =  {"one", "two", "three"};

        assertArrayEquals(expectedArray, resultArray);
    }
}