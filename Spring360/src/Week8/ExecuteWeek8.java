package Week8;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteWeek8 {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        Week8 ds1 = new Week8 ("The Flash", 25, 1000);
        Week8 ds2 = new Week8 ("Superman", 10, 500);
        Week8 ds3 = new Week8 ("Batman", 5, 250);
        Week8 ds4 = new Week8 ("Hulk", 2, 100);
        Week8 ds5 = new Week8 ("Thor", 1, 50);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}
