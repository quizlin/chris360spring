package Week8;

import java.util.Random;

public class Week8 implements Runnable {

    private String name;
    private int number;
    private int energy;
    private int rand;

    public Week8(String name, int number, int energy
    ) {

        this.name = name;
        this.number = number;
        this.energy = energy;

        Random random = new Random();
        this.rand = random.nextInt(1000);
    }

    public void run() {
        System.out.println("\n\nExecuting with these parameters: Name =" + name + " Number = "
                + number + " Energy = " + energy + " Rand Num = " + rand + "\n\n");
        for (int count = 1; count < rand; count++) {
            if (count % number == 0) {
                System.out.print(name + " is running! ");
                try {
                    Thread.sleep(energy);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n" + name + " is out of energy!\n\n");
    }
}
