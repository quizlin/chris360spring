package WeekFive;

import org.junit.jupiter.api.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WeekFive {

    Calculator calc = new Calculator();




    @Test
  @DisplayName("Assert Array Equal Test Case")
    public void assertArrayEqualTest(){
        Assertions.assertArrayEquals(new int[] {1,2,3}, new int[] {1,2,3});
    }


    @Test
   @DisplayName("Assert Not Null Test")
    public void assertNotNullTest(){
        String nullString = null;
        String notNullString = "ABC";
        Assertions.assertNotNull(notNullString);
    }

    @Test
    @DisplayName("Assert True Test")
    public void assertTrueTest(){
        boolean trueBoolean = true;
        boolean falseBoolean = false;

        Assertions.assertTrue(trueBoolean, "Assert True Test Failed");
    }

    @Test
    @DisplayName("Assert False Test")
    public void assertFalseTest(){
        boolean trueBoolean = true;
        boolean falseBoolean = false;
        Assertions.assertFalse(falseBoolean, "Assert False Test Failed");
    }



    @Test
    @DisplayName("Assert Equals Test")
    public void assertAllTest(){
        Assertions.assertAll(
                () -> Assertions.assertEquals(4, calc.add(1,3)),
                () -> Assertions.assertEquals(2, calc.add(-1,3)),
                () -> Assertions.assertEquals(-4, calc.add(-1,-3)),
                () -> Assertions.assertEquals(3,  calc.add(0,3)));

    }

    @Test
    @DisplayName("Assert Not Same Test")
    public void assertNotSameTest(){

        String str1=new String("Apple");
        String str2=new String("Apple");
        Assertions.assertNotSame(str1,str2);

    }



    @Test
    @DisplayName("Assert Null Test")
    public void assertNullTest(){


        String str1 = null;
        String str2 = "Fish";

        Assertions.assertNull(str1);
    }

    @Test
    @DisplayName("Assert Same Test")
    public void assertSameTest(){

        String str1="India";
        String str2="India";
        Assertions.assertSame(str1,str2);

    }

    @Test
    @DisplayName("Assert That Test")
    public void assertThatTest(){

        int expected = 0;
        int actual = 0;
        assertThat(expected, is(equalTo(actual)));

    }


}
