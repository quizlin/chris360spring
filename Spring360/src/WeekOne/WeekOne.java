package WeekOne;
import java.util.*;

    public class WeekOne {

        public static void main(String[] args) {

            System.out.println("-- List --");
            List list = new ArrayList();
            list.add("Star Trek");
            list.add("Strange New Worlds");
            list.add("comes out");
            list.add("May");
            list.add("5");
            list.add("2022");

            for (Object str : list) {
                System.out.println((String)str);
            }

            System.out.println("-- Queue --");
            Queue queue = new PriorityQueue();
            queue.add("world");
            queue.add("my");
            queue.add("name");
            queue.add("is");
            queue.add("chris");
            queue.add("Hello");

            Iterator iterator = queue.iterator();
            while (iterator.hasNext()) {
                System.out.println(queue.poll());
            }


            System.out.println("-- Set --");
            Set set = new TreeSet();
            set.add("Hello");
            set.add("everyone");
            set.add("Star Wars & Star Trek");
            set.add("are");
            set.add("cool");
            set.add("Hello");

            for (Object str : set) {
                System.out.println((String)str);
            }



            System.out.println("-- Map --");
            Map map = new HashMap();
            map.put(1,"One");
            map.put(2,"Two");
            map.put(3,"Three");
            map.put(4,"Four");
            map.put(5,"Five");
            map.put(3,"Six");

            for (int i = 1; i < 6; i++) {
                String result = (String)map.get(i);
                System.out.println(result);
            }

        }
    }