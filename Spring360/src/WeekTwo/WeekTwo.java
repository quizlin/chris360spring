package WeekTwo;


import java.util.Scanner;

public class WeekTwo {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Input first number: ");
        int num1 = in.nextInt();

        System.out.print("Input second number: ");
        int num2 = in.nextInt();
            while(num2 == 0) {
                System.out.println("ERROR Please enter a valid number (not Zero)");
                System.out.println("");
                System.out.println("Input second number: ");
                num2 = in.nextInt();
            }


        System.out.println(num1 + " / " + num2 + " = " + num1 / num2);
    }

}
