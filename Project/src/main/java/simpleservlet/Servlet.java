
package simpleservlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;


@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");


        String meal = request.getParameter("meal");
        if(meal.matches("burger|salad|nuggets"))
            out.println("");
        else {
            out.println("That choice of Meal is not available, please click back on your browser and input one of the available options! ");
        }


        String drink = request.getParameter("drink");
        if(drink.matches("coke|fanta|water"))
            out.println("");
        else {
            out.println("That choice of Drink is not available, please click back on your browser and input one of the available options! ");
        }


        String side = request.getParameter("side");
        if(side.matches("fries|onion rings|fruit"))
            out.println("");
        else {
            out.println("That choice of Side is not available, please click back on your browser and input one of the available options! ");
        }

        String dessert = request.getParameter("dessert");
        if(dessert.matches("ice cream|pie|chocolate"))
            out.println("");
        else {
            out.println("That choice of Dessert is not available, please click back on your browser and input one of the available options! ");
        }


        int mealp = 0;
        int drinkp = 0;
        int sidep = 0;
        int dessertp = 0;
        int total = 0;

        if(dessert.matches("ice cream"))
            dessertp = 5;
        if(dessert.matches("pie"))
            dessertp = 6;
        if(dessert.matches("chocolate"))
            dessertp = 3;

        if(meal.matches("burger"))
            mealp = 4;
        if(meal.matches("salad"))
            mealp = 4;
        if(meal.matches("nuggets"))
            mealp = 5;

        if(drink.matches("coke"))
            drinkp = 1;
        if(drink.matches("fanta"))
            drinkp = 1;
        if(drink.matches("water"))
            drinkp = 0;

        if(side.matches("fries"))
            sidep = 3;
        if(side.matches("onion rings"))
            sidep = 3;
        if(side.matches("fruit"))
            sidep = 2;

        total = mealp + drinkp + sidep + dessertp;


        out.println("<h1>Your order is:</h1>");
        out.println("<p>Meal: " + meal + "</p>");
        out.println("<p>Drink: " + drink + "</p>");
        out.println("<p>Side: " + side + "</p>");
        out.println("<p>Dessert: " + dessert + "</p>");

        out.println("<p>Your total is: $" + total + "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }

}